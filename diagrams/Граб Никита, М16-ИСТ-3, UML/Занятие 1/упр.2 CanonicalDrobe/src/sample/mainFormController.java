package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class mainFormController {
    @FXML
    private TextField nominator;
    @FXML
    private TextField denominator;
    @FXML
    private void getDouble() throws CCanonicalDrobe.ZeroDenominatorException {

        double drobe = Main.getDrobe(Integer.parseInt(nominator.getText()), Integer.parseInt(denominator.getText()));
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("Ответ");
        alert.setHeaderText(null);
        alert.setContentText("Дробь в виде double числа имеет вид "+drobe);

        alert.showAndWait();
    };
}

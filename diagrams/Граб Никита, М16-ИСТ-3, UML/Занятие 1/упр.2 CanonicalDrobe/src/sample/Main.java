package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("mainForm.fxml"));
        primaryStage.setTitle("Каноническая дробь");
        primaryStage.setScene(new Scene(root, 400, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static double getDrobe(int nominator, int denominator) throws CCanonicalDrobe.ZeroDenominatorException{
        try {
            CCanonicalDrobe drobe = new CCanonicalDrobe(nominator, denominator);
            return drobe.getDouble();
        } catch (CCanonicalDrobe.ZeroDenominatorException ex ) {
            System.out.print("Не удалось.");
            return 0;
        }
    }
}

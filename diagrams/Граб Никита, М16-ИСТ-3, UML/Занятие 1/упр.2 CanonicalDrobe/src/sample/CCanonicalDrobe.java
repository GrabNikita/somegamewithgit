package sample;

/**
 * Created by Deadbox583 on 17.11.2016.
 */
public class CCanonicalDrobe {
    private int nominator;
    private int denominator;

    CCanonicalDrobe(int nominator, int denominator) throws ZeroDenominatorException{
        setDrobe(nominator,denominator);
    }
    public int getDenominator() {
        return denominator;
    }

    public int getNominator() {
        return nominator;
    }

    public void setDenominator(int denominator) throws ZeroDenominatorException {
        if(denominator == 0) throw new ZeroDenominatorException();
        else this.denominator = denominator;
    }

    public void setNominator(int nominator) {
        this.nominator = nominator;
    }

    public void setDrobe(int nominator, int denominator) throws ZeroDenominatorException{
        setNominator(nominator);
        setDenominator(denominator);
    }
    public double getDouble(){
        return (double) nominator/(double) denominator;
    }

    public class ZeroDenominatorException extends Exception {
        ZeroDenominatorException(){
            System.out.println("Denominator is Zero");
        }
    }
}

package mainPkg;

/**
 * Created by Deadbox583 on 13.11.2016.
 */
public class CShip implements IVehicle {

    public double MAX_SPEED_SHIP = 80.00;
    private String name;
    private double maxSpeed;
    @Override
    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) throws CShipException {
        if (maxSpeed > MAX_SPEED_SHIP)
            throw new CShipException();
        else {
            this.maxSpeed = maxSpeed;
        }
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    CShip(String name, double maxSpeed) throws CShipException {
        this.setName(name);
        this.setMaxSpeed(maxSpeed);
    }

        // для быстроты пусть будет статик
        public static class CShipException extends Exception {
            CShipException(){
                System.out.print("no");
            }
        }
}

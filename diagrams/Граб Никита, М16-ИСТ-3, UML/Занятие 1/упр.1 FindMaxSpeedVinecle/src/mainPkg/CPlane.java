package mainPkg;

/**
 * Created by Deadbox583 on 13.11.2016.
 */
public class CPlane implements IVehicle {

    private double maxSpeed;
    private String name;
    public static final double MAX_PLANE_SPEED = 1200.00;
    @Override
    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) throws CPlaneException {
        if (maxSpeed> MAX_PLANE_SPEED) throw new CPlaneException();
        else {
            this.maxSpeed = maxSpeed;
        }
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public class CPlaneException extends Exception {
        CPlaneException(){
            System.out.print("Упс ");
        }
    }
    public CPlane(String name, double maxSpeed) throws CPlaneException {
        this.setName(name);
        this.setMaxSpeed(maxSpeed);
    }
}

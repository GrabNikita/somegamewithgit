package mainPkg;

/**
 * Created by Deadbox583 on 13.11.2016.
 */
public interface IVehicle {
    public double getMaxSpeed();
    public String getName();
}

package mainPkg;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;

public class mainPageController {
    @FXML
    private Button find;

    private static ArrayList<IVehicle> vehicles;
    private ObservableList<IVehicle> OLvehicles = FXCollections.observableArrayList();
    @FXML
    private TableView table;
    @FXML
    private TableColumn<IVehicle,String> nameColumn;
    @FXML
    private TableColumn<IVehicle,String> maxSpeedColumn;
    @FXML
    public void findMax(){
        vehicles = Main.getVehicles();
        IVehicle vehicleWithMaxSpeed = vehicles.get(0);
        for (IVehicle vehicle :vehicles) {
            if(vehicle.getMaxSpeed()>vehicleWithMaxSpeed.getMaxSpeed()) {
                vehicleWithMaxSpeed = vehicle;
            }
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle("Ответ");
        alert.setHeaderText(null);
        alert.setContentText("Самую большую макс скорость имеет "+vehicleWithMaxSpeed.getName());

        alert.showAndWait();
    }

    @FXML
    private void initialize() {
        // Инициализация таблицы адресатов с двумя столбцами.
        vehicles = Main.getVehicles();
        for (IVehicle vehicle: vehicles) {
            OLvehicles.add(vehicle);
        }
        nameColumn.setCellValueFactory(new PropertyValueFactory<IVehicle, String>("name"));
        maxSpeedColumn.setCellValueFactory(new PropertyValueFactory<IVehicle, String>("maxSpeed"));
        table.setItems(OLvehicles);
    }
}

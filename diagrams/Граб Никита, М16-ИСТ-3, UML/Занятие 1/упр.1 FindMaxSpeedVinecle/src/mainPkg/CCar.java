package mainPkg;

/**
 * Created by Deadbox583 on 13.11.2016.
 */
public class CCar implements IVehicle {
    public static final double MAX_SPEED_CAR = 200.00;
    private double maxSpeed;
    private String name;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) throws CCarException {
        if(maxSpeed >= MAX_SPEED_CAR) throw new CCarException();
        else {
            this.maxSpeed = maxSpeed;
        }
    }

    public CCar(String name, double maxSpeed) throws CCarException {
        this.setMaxSpeed(maxSpeed);
        this.setName(name);
    }

    public class CCarException extends Exception {
        CCarException() {
            System.out.print("Упс");
        }
    }
}

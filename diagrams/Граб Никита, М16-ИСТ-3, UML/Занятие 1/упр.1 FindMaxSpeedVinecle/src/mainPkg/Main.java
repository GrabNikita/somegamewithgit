package mainPkg;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.*;

public class Main extends Application {

    private static ArrayList<IVehicle> vehicles = new ArrayList<IVehicle>();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("main_page.fxml"));
        primaryStage.setTitle("Нахождение транспорта с макс скоростью");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    public static void main(String[] args) throws CCar.CCarException, CPlane.CPlaneException, CShip.CShipException {
        try {
            vehicles.add(new CCar("bmw", 100.00));
            vehicles.add(new CPlane("plane1", 1000.00));
            vehicles.add(new CShip("battleship", 50.00));
        } catch (CCar.CCarException | CPlane.CPlaneException | CShip.CShipException ex) {
            System.out.print("не удалось создать транспорт");
        }
        launch(args);
    }

    public static ArrayList<IVehicle> getVehicles() {
        return vehicles;
    }

}

package Controller;

import Main.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainMenuController {


    @FXML
    public void play(ActionEvent event) throws Exception {
        if(Main.createPlayer == null) {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("../View/CreatePlayer.fxml"));
            stage.setTitle("SomeGame");
            stage.setScene(new Scene(root, 800, 600));
            stage.show();
            Main.createPlayer = stage;
        }
        else {
            Main.createPlayer.show();
        }
        Main.mainMenu.close();
    }
    @FXML
    public void exit() {
        Platform.exit();
    }
}

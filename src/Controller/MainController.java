package Controller;


import Main.Main;
import Model.CPlayer;
import Model.CTavern;
import Model.ILocation;
import Model.INPC;
import Model.MyFactoryPattern.CSitizenFactory;
import Model.MyFactoryPattern.CThieveFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.util.ArrayList;

public class MainController {

    public static ArrayList<INPC> NPCes = new ArrayList<INPC>();
    public static CSitizenFactory SitizenFactory = new CSitizenFactory();
    public static CThieveFactory ThieveFactory = new CThieveFactory();
    public static ArrayList<ILocation> Locations = new ArrayList<ILocation>();

    @FXML
    TextArea log;

    @FXML
    ComboBox<String> target;
    private void addTextToLog(String text) {
        log.appendText("\n" + text);
        log.setScrollTop(Double.MAX_VALUE);
    }
    @FXML
    private void initialize() {
        Locations.add(new CTavern());
        ILocation tavern = Locations.get(0);
        tavern.setName("Таверна \"Кабанчик\"");
        tavern.setFirstLook("Обычная таверна каких много с большим обеденным залом и камином в дальнем конце, " +
                "около которого расположена барная стойка.");
        tavern.setDescription("В одном углу кучкуются картежники, в другом крепкие ребята, меряющиеся силой." +
                " Сегодня довольно людно. Ведь сегодня канун Нового года, а значит все хотят хорошенько это отпраздновать." +
                " А кто-то судя по всему уже достаточно хорошо отпраздновал и теперь мирно посапывает прямо за столом.");
        tavern.setBigDescription("Вы замечаете как бармен, явно недовольно поглядывает на одного набравщегося посетителя," +
                " который пристает к остальным с явным намерением получить бесплатную выпивку.");
        tavern.setNPCList( new ArrayList<INPC>());
        tavern.addNPC(SitizenFactory.createNPC(
                "Трактирщик Осмунд",
                "Хозяин этой таверны. Лысоватый мужчина лет сорока. Одет в заляпанный фартук поверх обычной одежды.",
                "Но вас не обманет его кажущийся безобидным вид. Этот человек явно не против" +
                        " разбавить ваш эль, если это позволит заработать на этом пару лишних монет. Да и к тому, он " +
                        "именно тот, кто всегда в курсе всех последних событий в городе. С ним неплохо дружить. " +
                        "Бесплатно пива не нальет, но может поделиться информацией.",
                0,
                'м'
        ));
        tavern.addNPC(SitizenFactory.createNPC(
                "Выпивоха Джери",
                "Изрядно поднабравшийся посетитель таверны, но кажется ещё не осознавший, что ему уже хватит.",
                "Вы узнаете в нем местного пьянчугу по имени Джери. Этот пройдоха не так прост как может показаться на первый взгляд.",
                1000,
                'м'
        ));
        tavern.addNPC(SitizenFactory.createNPC(
                "Джайна Праудмур",
                "Обычная девушка, работающая служанкой у местного мага.",
                "От вас не укрылись взгляды, которые она бросает на Артаса.",
                30,
                'ж'
        ));
        tavern.addNPC(SitizenFactory.createNPC(
                "Артас Менетил",
                "Паренек, помогающий на кухне Гимли.",
                "Всё время поглядывает в сторону соревнующихся в силе мужчин, но судя по всему слишком робок... или беден, чтобы попытать счастья самому.",
                20,
                'м'
        ));
        tavern.addNPC(SitizenFactory.createNPC(
                "Гром Задира",
                "Здоровенный детина, работающий на лесопилке. Говорят может одним ударом топора свалить небольшое деревце.",
                "И глядя на него в это легко верится. Уже давно держит титул метсного чемпиона по армреслингу.",
                50,
                'м'
        ));
        tavern.addNPC(SitizenFactory.createNPC(
                "Гимли",
                "Держит собственную пекарню. И кажется сегодня он уже не будет ничего печь. И завтра утром тоже. А жаль, у него неплохой хлеб получается.",
                "На его поясе виден обрезок ремешка на том месте, где должен быть его кошель. Кажется я зашел именно туда куда нужно. Тот кого я ищу здесь.",
                0,
                'м'
        ));
        tavern.addNPC(SitizenFactory.createNPC(
                "Лайла",
                "Обычная девушка.",
                "Сегодня она необычайно груста. Что же такое могло её так расстроить?",
                0,
                'ж'
        ));
        tavern.addNPC(ThieveFactory.createNPC(
                "Джон",
                "Обычный парень.",
                "Как всегда весел. Ниразу не видел его грустным, и меня этот факт ничанает беспокоить.",
                20,
                'м'
        ));
        CPlayer.getInstance().setCurrentLocation(tavern);
        for ( INPC npc: CPlayer.getInstance().getCurrentLocation().getNPCList()){
            CPlayer.getInstance().attach(npc);
        }
        addTextToLog(CPlayer.getInstance().getCurrentLocation().getFirstLook());
    }

    @FXML
    public void inspect_location() {
        addTextToLog(CPlayer.getInstance().inspectLocation(
                CPlayer.getInstance().getCurrentLocation()
        ));
        for ( INPC npc: CPlayer.getInstance().getCurrentLocation().getNPCList()){
            target.getItems().add(npc.getName());
        }
    }
    @FXML
    public void attack() {
        for(INPC npc: CPlayer.getInstance().getCurrentLocation().getNPCList()){
            if(npc.getName() == target.getSelectionModel().getSelectedItem()) {
                addTextToLog(CPlayer.getInstance().Attack(npc));
                break;
            }
        }
    }
    @FXML
    public void range_attack() {
        for(INPC npc: CPlayer.getInstance().getCurrentLocation().getNPCList()){
            if(npc.getName() == target.getSelectionModel().getSelectedItem()) {
                addTextToLog(CPlayer.getInstance().rangeAttack(npc));
                break;
            }
        }
    }
    @FXML
    public void inspect_npc() {
        for(INPC npc: CPlayer.getInstance().getCurrentLocation().getNPCList()){
            if(npc.getName() == target.getSelectionModel().getSelectedItem()) {
                addTextToLog(CPlayer.getInstance().inspectNPC(npc));
                break;
            }
        }
    }
    @FXML
    public void hide() {
        addTextToLog(CPlayer.getInstance().hide());
    }
    @FXML
    public void showmenu(ActionEvent event) throws Exception {
        if (Main.mainMenu == null) {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("../View/MainMenu.fxml"));
            stage.setTitle("SomeGame");
            stage.setScene(new Scene(root, 800, 600));
            stage.show();
        }
        else Main.mainMenu.show();
        Main.createPlayer.close();
    }
}

package Controller;

import Main.Main;
import Model.CPlayer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CreatePlayerController {
    @FXML
    TextField strength;
    @FXML
    TextField dexterity;
    @FXML
    TextField constitution;
    @FXML
    TextField intelligence;
    @FXML
    TextField wisdow;
    @FXML
    TextField charisma;
    @FXML
    TextField name;
    @FXML
    TextField s_name;

    @FXML
    public void showmenu(ActionEvent event) throws Exception {
        if (Main.mainMenu == null) {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("../View/MainMenu.fxml"));
            stage.setTitle("SomeGame");
            stage.setScene(new Scene(root, 800, 600));
            stage.show();
        }
        else Main.mainMenu.show();
        Main.createPlayer.close();
    }
    @FXML
    public void play() throws Exception {
        CPlayer.getInstance().setName(name.getText());
        CPlayer.getInstance().setSecond_name(s_name.getText());
        CPlayer.getInstance().setStrength(Integer.parseInt(strength.getText()));
        CPlayer.getInstance().setDexterity(Integer.parseInt(dexterity.getText()));
        CPlayer.getInstance().setConstitution(Integer.parseInt(constitution.getText()));
        CPlayer.getInstance().setIntelligence(Integer.parseInt(intelligence.getText()));
        CPlayer.getInstance().setWisdom(Integer.parseInt(wisdow.getText()));
        CPlayer.getInstance().setCharisma(Integer.parseInt(charisma.getText()));
        CPlayer.getInstance().setGold(30);
        CPlayer.getInstance().setArmor(0);
        CPlayer.getInstance().setMaxHitPoints(10);
        CPlayer.getInstance().setCurrentHitPoints(
                CPlayer.getInstance().getMaxHitPoints()
        );

        if(Main.mainView == null) {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("../View/MainView.fxml"));
            stage.setTitle("SomeGame");
            stage.setScene(new Scene(root, 800, 600));
            stage.show();
            Main.mainView = stage;
        }
        else {
            Main.mainView.show();
        }
        Main.createPlayer.close();
    }
}

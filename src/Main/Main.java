package Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage mainMenu;
    public static Stage createPlayer;
    public static Stage mainView;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../View/MainMenu.fxml"));
        primaryStage.setTitle("SomeGame");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
        mainMenu = primaryStage;

    }


    public static void main(String[] args) {
        launch(args);
    }
}

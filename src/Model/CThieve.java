package Model;

/**
 * Created by Deadbox583 on 27.12.2016.
 */
public class CThieve extends CSitizen {

    public String steal(INPC target) {
        int stolen_summ = (int)(target.getGold()/10);
        target.setGold(target.getGold() - stolen_summ);
        return "Было украдено " + stolen_summ + " монет.";
    }
}

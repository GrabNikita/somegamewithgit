package Model.MyObserverPattern;

/**
 * Created by Deadbox583 on 26.12.2016.
 */
public interface IPlayerObserver {

    public String notifyHide(boolean is_hidden);
    public String notifyAudio(String message);
}

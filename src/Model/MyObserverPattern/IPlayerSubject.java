package Model.MyObserverPattern;

/**
 * Created by Deadbox583 on 26.12.2016.
 */
public interface IPlayerSubject {

    public void attach(IPlayerObserver observer);
    public void detach(IPlayerObserver observer);
    public void notifyAllAboutHide(boolean is_hidden);
    public void notifyAllAboutAudio(String message);
}

package Model;

import Model.MyObserverPattern.IPlayerObserver;

import java.util.Observer;

/**
 * Created by Deadbox583 on 25.12.2016.
 */
public interface INPC extends IPlayerObserver {


    public void setName(String name);
    public String getName();

    /**
     *
     * @param male - задает пол персонажа строка "м" или "ж"
     */
    public void setMale(char male);
    public char getMale();
    /*
    * +++ Dungeon World +++
    */
    public int takeDamage();
    public String toGetDamage(int damage);

    public String getDescription();
    public String getBigDescription();

    public void setDescription(String description);
    public void setBigDescription(String bigDescription);

    public int getGold();
    public void setGold(int gold);
    /*
    * --- Dungeon World ---
    */
}

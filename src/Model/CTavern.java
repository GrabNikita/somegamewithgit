package Model;

import java.util.ArrayList;

/**
 * Created by Deadbox583 on 27.12.2016.
 */
public class CTavern implements ILocation {

    String name;
    String description;
    String bigDescription;
    String firstLook;
    ArrayList<INPC> NPCList;

    /*
    * +++ Getters and Setters +++
    */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBigDescription(String bigDescription) {
        this.bigDescription = description + "\n" + bigDescription;
    }

    @Override
    public String getBigDescription() {
        return bigDescription;
    }

    @Override
    public void setFirstLook(String firstLook) {
        this.firstLook = firstLook;
    }

    @Override
    public String getFirstLook() {
        return firstLook;
    }

    @Override
    public void setNPCList(ArrayList<INPC> NPCList) {
        this.NPCList = NPCList;
    }

    @Override
    public ArrayList<INPC> getNPCList() {
        return NPCList;
    }
    /*
    * --- Getters and Setters ---
    */

    @Override
    public void addNPC(INPC NPC){
        NPCList.add(NPC);
    };

    @Override
    public void removeNPC(INPC NPC) {
        NPCList.remove(NPC);
    }
}

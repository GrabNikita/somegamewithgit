package Model;

import Model.DiceEmulator.C1D4Thrown;
import Model.DiceEmulator.IThrown;

/**
 * Created by Deadbox583 on 26.12.2016.
 */
public class CSitizen implements INPC {

    private String name;
    private char male;
    /*
    * +++ Dungeon World +++
    */
    private int MaxHitPoints;
    private int CurrentHitPoints;
    private int Armor;

    private String description;
    private String bigDescription;
    private int gold;

    private IThrown damageDice = new C1D4Thrown();

    /*
    * --- Dungeon World ---
    */
    @Override
    public int takeDamage() {
        return damageDice.getTrownResult(0);
    }

    @Override
    public String notifyHide(boolean is_hidden) {
        if(is_hidden) {
            return "I am not see player.";
        }
        else {
            return "WTF? Why he crawl on the ground like a worm?";
        }
    }

    @Override
    public String notifyAudio(String message) {
        return "Who say \'" + message + "\'?";
    }

    @Override
    public String toGetDamage(int damage) {
        CurrentHitPoints -= damage - Armor;
        return  name + " получил люлей на " + damage  + " урона.";
    }

    /*
    * +++ Getters and Setters +++
    */

    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String getName() {
        return name;
    }
    @Override
    public String getDescription() {
        return description;
    }
    @Override
    public String getBigDescription() {
        return description + "\n" + bigDescription;
    }
    @Override
    public void setDescription(String description) {
        this.description = description;
    }
    @Override
    public void setBigDescription(String bigDescription) {
        this.bigDescription = bigDescription;
    }
    @Override
    public char getMale() {
        return male;
    }
    @Override
    public void setMale(char male) {
        this.male = male;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
    public int getGold() {
        return gold;
    }

    public int getArmor() {
        return Armor;
    }

    public int getCurrentHitPoints() {
        return CurrentHitPoints;
    }

    public int getMaxHitPoints() {
        return MaxHitPoints;
    }

    public void setArmor(int armor) {
        Armor = armor;
    }

    public void setCurrentHitPoints(int currentHitPoints) {
        CurrentHitPoints = currentHitPoints;
    }

    public void setDamageDice(IThrown damageDice) {
        this.damageDice = damageDice;
    }

    public void setMaxHitPoints(int maxHitPoints) {
        MaxHitPoints = maxHitPoints;
    }
    /*
    * --- Getters and Setters ---
    */
}

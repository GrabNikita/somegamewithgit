package Model.DiceEmulator;

/**
 * Created by Deadbox583 on 25.12.2016.
 */
public interface IThrown {

    public final String THROWN_NAME = "interface";

    /**
     * @return результат броска
     */
    public int getTrownResult(int modifier);
}

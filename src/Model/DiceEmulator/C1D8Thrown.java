package Model.DiceEmulator;

/**
 * Created by Deadbox583 on 25.12.2016.
 */
public class C1D8Thrown implements IThrown {

    public final String THROWN_NAME = "1D8";

    @Override
    public int getTrownResult(int modifier) {
        return CDice.getResult(8) + modifier;
    }
}

package Model.DiceEmulator;

/**
 * Created by Deadbox583 on 27.12.2016.
 */
public class C1D4Thrown implements IThrown {
    public final String THROWN_NAME = "1D4";

    @Override
    public int getTrownResult(int modifier) {
        return CDice.getResult(4) + modifier;
    }
}

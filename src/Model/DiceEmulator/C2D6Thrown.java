package Model.DiceEmulator;

/**
 * Created by Deadbox583 on 25.12.2016.
 */
public class C2D6Thrown implements IThrown {

    public final String THROWN_NAME = "2D6";

    @Override
    public int getTrownResult(int modifier) {
        return CDice.getResult(6) + CDice.getResult(6) + modifier;
    }
}

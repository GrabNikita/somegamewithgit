package Model;

import Model.DiceEmulator.C1D8Thrown;
import Model.DiceEmulator.C2D6Thrown;
import Model.MyObserverPattern.IPlayerObserver;
import Model.MyObserverPattern.IPlayerSubject;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Deadbox583 on 25.12.2016.
 */
public class CPlayer implements IPlayerSubject {


    /*
    * +++ Architecture +++
    */
    /**
     * @apiNote for Singleton pattern
     */
    private static CPlayer instance;

    /**
     * @apiNote for Observer pattern - хранит перечень наблюдателей Player'a
     */
    private ArrayList<IPlayerObserver> Observers = new ArrayList<IPlayerObserver>();

    /**
     * @apiNote определяет прячется герой в данный момент или нет.
     */
    private boolean hideState;
    /*
    * --- Architecture ---
    */

    private String name;
    private String second_name;
    private ILocation currentLocation;

    /*
    * +++ Dungeon World +++
    */
    private int strength;
    private int dexterity;
    private int constitution;
    private int intelligence;
    private int wisdom;
    private int charisma;

    private int MaxHitPoints;
    private int CurrentHitPoints;
    private int Armor;
    private int MaxWeight;
    private int CurrentWeight;
    private int gold;

    /*
    * --- Dungeon World ---
    */


    private CPlayer() {

    }

    /*
    * +++ Architecture +++
    */
    public static synchronized CPlayer getInstance() {
        if (instance == null) {
            instance = new CPlayer();
        }
        return instance;
    }

    @Override
    public void attach(IPlayerObserver observer) {
        Observers.add(observer);
    }

    @Override
    public void detach(IPlayerObserver observer) {
        Observers.remove(observer);
    }

    @Override
    public void notifyAllAboutAudio(String message) {
        for (IPlayerObserver observer : Observers) {
            observer.notifyAudio(message);
        }
    }

    @Override
    public void notifyAllAboutHide(boolean is_hidden) {
        for (IPlayerObserver observer : Observers) {
            observer.notifyHide(is_hidden);
        }
    }

    /*
    * --- Architecture ---
    */

    /*
    * +++ Dungeon World +++
    */

    /**
     * @param characteristic_value - значение определенной характеристики персонажа (strangth, dexterity, constitution, intelligence, wisdom, charisma) may be between 8 and 18
     * @return modifier of characteristic - модификатор характеристики. may be between -1 and 3
     * @
     */
    public int getCharacteristicModifier(int characteristic_value) {
        if (characteristic_value == 18)
            return 3;
        else if (characteristic_value >= 16)
            return 2;
        else if (characteristic_value >= 13)
            return 1;
        else if (characteristic_value >= 9)
            return 0;
        else return -1;
    }

    /**
     * @param target - цель атаки
     * @return - строку описания результата действия
     */
    public String Attack(INPC target) {
        C2D6Thrown attack_roll = new C2D6Thrown();
        int attack_result = attack_roll.getTrownResult(
                getCharacteristicModifier(strength)
        );
        if (attack_result >= 10) {
            return "Полный успех" + giveDamage(target);
        } else if (attack_result >= 7) {
            return "Полууспех\n" + giveDamage(target) + toGetDamage(target.takeDamage());
        } else {
            return "Провал. АААААХАХАХАХАХААХАХАХАХХАХАХАХАХ" + toGetDamage(target.takeDamage());
        }

    }

    /**
     * @param target - цель атаки
     * @return - строку описания результата действия
     */
    public String rangeAttack(INPC target) {
        C2D6Thrown attack_roll = new C2D6Thrown();
        int attack_result = attack_roll.getTrownResult(
                getCharacteristicModifier(dexterity)
        );
        if (attack_result >= 10) {
            return "Полный успех" + giveDamage(target);
        } else if (attack_result >= 7) {
            return "Полууспех" + giveDamage(target) + toGetDamage(target.takeDamage());
        } else {
            return "Провал. АААААХАХАХАХАХААХАХАХАХХАХАХАХАХ" + toGetDamage(target.takeDamage());
        }

    }

    public String hide() {
        C2D6Thrown hide_roll = new C2D6Thrown();
        int hide_result = hide_roll.getTrownResult(
                getCharacteristicModifier(dexterity)
        );
        if (hide_result >= 10) {
            hideState = true;
            notifyAllAboutHide(hideState);
            return "Вы скрылись от всех.";
        } else if (hide_result >= 7) {
            hideState = true;
            Random random = new Random();
            String tmp_return = "";
            for (IPlayerObserver observer : Observers) {
                tmp_return += observer.notifyHide(random.nextBoolean()) + "\n";
            }
            return "Полууспех \n" + tmp_return;
        } else {
            hideState = false;
            String tmp_return = "";
            for (IPlayerObserver observer : Observers) {
                tmp_return += observer.notifyHide(false) + "\n";
            }
            return "Провал. АААААХАХАХАХАХААХАХАХАХХАХАХАХАХ \n" + tmp_return;
        }

    }

    public String inspectNPC(INPC target) {
        C2D6Thrown inspect_roll = new C2D6Thrown();
        int inspect_result = inspect_roll.getTrownResult(
                getCharacteristicModifier(wisdom)
        );
        if (inspect_result >= 10) {
            return "Я узнал, о " + target.getName() + "следующее: \"" + target.getBigDescription() + "\". Надеюсь мне пригодится эта информация.";
        } else if (inspect_result >= 7) {
            return "Я узнал, следующее \"" + target.getDescription() + "\". Хмм...";
        } else {
            if (target.getMale() == 'ж') {
                return "Кажется герой засмотрелся на декольте и был замечен, за что выхватил пощечину.\n" + toGetDamage(1);
            } else {
                return "Взгляд героя был замечен. Ему пришлось притвориться, что они знакомы и проставить кружку пива.\n" + setGold(getGold() - 10);
            }
        }
    }

    public String inspectLocation(ILocation target) {
        C2D6Thrown inspect_roll = new C2D6Thrown();
        int inspect_result = inspect_roll.getTrownResult(
                getCharacteristicModifier(wisdom)
        );
        if (inspect_result >= 10) {
            return "Я узнал, о " + target.getName() + "следующее: \"" + target.getBigDescription() + "\". Надеюсь мне пригодится эта информация.";
        } else if (inspect_result >= 7) {
            return "Я узнал, следующее \"" + target.getDescription() + "\". Хмм...";
        } else {
            return "Засмотревшись на огонь очаге споткнулся и упал на гладкую твердую поверхность своей мягкой лицевой поверхностью.\n" + toGetDamage(1);
        }
    }

    /**
     * @param target - цель, которая получит урон
     */
    public String giveDamage(INPC target) {
        C1D8Thrown damage_roll = new C1D8Thrown();
        return "Получи, ты, NPC!\n" + target.toGetDamage(damage_roll.getTrownResult(0));
    }

    public String toGetDamage(int damage) {
        String tmp_return = "Игрок получил люлей на " + (damage - Armor) + " очков жизней.\n" + setCurrentHitPoints(getCurrentHitPoints() - (damage - Armor));
        if(getCurrentHitPoints() <= 0) return tmp_return + "Персонаж умер. Конец игры";
        return tmp_return;
    }
    /*
    * --- Dungeon World ---
    */


    /*
    * +++ Getters and Setters +++
    */
    public void setCurrentLocation(ILocation location) {
        this.currentLocation = location;
    }

    public ILocation getCurrentLocation() {
        return currentLocation;
    }

    public int getGold() {
        return gold;
    }

    public String setGold(int gold) {
        this.gold = gold;
        return "Теперь у меня " + gold + " монет.";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public boolean isHideState() {
        return hideState;
    }

    public void setHideState(boolean hideState) {
        this.hideState = hideState;
    }

    public int getCharisma() {
        return charisma;
    }

    public int getConstitution() {
        return constitution;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }


    public int getArmor() {
        return Armor;
    }

    public int getCurrentHitPoints() {
        return CurrentHitPoints;
    }

    public int getCurrentWeight() {
        return CurrentWeight;
    }

    public int getMaxHitPoints() {
        return MaxHitPoints;
    }

    public int getMaxWeight() {
        return MaxWeight;
    }

    public void setArmor(int armor) {
        Armor = armor;
    }

    public String setCurrentHitPoints(int currentHitPoints) {
        CurrentHitPoints = currentHitPoints;
        return "Количество очков жизни героя стало " + getCurrentHitPoints() + ".";
    }

    public void setCurrentWeight(int currentWeight) {
        CurrentWeight = currentWeight;
    }

    public void setMaxHitPoints(int maxHitPoints) {
        MaxHitPoints = maxHitPoints;
    }

    public void setMaxWeight(int maxWeight) {
        MaxWeight = maxWeight;
    }
    /*
    * --- Getters and Setters ---
    */
}

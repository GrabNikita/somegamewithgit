package Model;

import java.util.ArrayList;

/**
 * Created by Deadbox583 on 27.12.2016.
 */
public interface ILocation {

    /*
    * +++ Getters and Setters +++
    */
    public String getName();
    public void setName(String name);

    public String getDescription();
    public void setDescription(String description);

    public String getBigDescription();
    public void setBigDescription(String bigDescription);

    public void setNPCList(ArrayList<INPC> NPCList);
    public ArrayList<INPC> getNPCList();

    public void setFirstLook(String firstLook);
    public String getFirstLook();
    /*
    * --- Getters and Setters ---
    */

    public void addNPC(INPC NPC);
    public void removeNPC(INPC NPC);
}

package Model.MyFactoryPattern;

import Model.CSitizen;
import Model.INPC;

/**
 * Created by Deadbox583 on 27.12.2016.
 */
public class CSitizenFactory implements INPCFactory {
    @Override
    public INPC createNPC(String name, String description, String bigDescription, int gold, char male) {
        INPC tmp_npc = new CSitizen();
        tmp_npc.setName(name);
        tmp_npc.setDescription(description);
        tmp_npc.setBigDescription(bigDescription);
        tmp_npc.setGold(gold);
        tmp_npc.setMale(male);
        return tmp_npc;
    }
}

package Model.MyFactoryPattern;

import Model.INPC;

/**
 * Created by Deadbox583 on 27.12.2016.
 */
public interface INPCFactory {
    public INPC createNPC(String name, String description, String bigDescription, int gold, char male);
}

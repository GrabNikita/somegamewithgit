package Model.MyFactoryPattern;

import Model.CThieve;
import Model.INPC;

/**
 * Created by Deadbox583 on 28.12.2016.
 */
public class CThieveFactory implements INPCFactory {
    @Override
    public INPC createNPC(String name, String description, String bigDescription, int gold, char male) {
        INPC tmp_npc = new CThieve();
        tmp_npc.setName(name);
        tmp_npc.setDescription(description);
        tmp_npc.setBigDescription(bigDescription);
        tmp_npc.setGold(gold);
        tmp_npc.setMale(male);
        return tmp_npc;
    }
}
